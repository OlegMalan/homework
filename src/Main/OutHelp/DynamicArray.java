package Main.OutHelp;

public class DynamicArray {

    private int[] data;
    private int size;

    public DynamicArray() {
        data = new int[1]; // Початковий розмір масиву
        size = 0;
    }

    public void add(int value) {
        if (size == data.length) {// перевіряємо розмір
            int[] newData = new int[data.length * 2];
            System.arraycopy(data, 0, newData, 0, data.length);// та розширює масив
            data = newData;
        }
        data[size++] = value;
    }


    public int get(int index) {
        if (index < 0 || index >= size) {   // тут ми перевіряємо що б число вписалося в масив
            throw new IndexOutOfBoundsException();// і поступово додаємо до масиву
        }
        return data[index];
    }

    public int size() {
        return size;
    }
}
