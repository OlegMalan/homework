package Main.Game;
import Main.OutHelp.DynamicArray;
import Main.RandomNum.RandomNumGen;
import Main.User.User;
import java.util.Scanner;

public class GameStart {
    public static void miniGame(){

        Scanner scanner = new Scanner(System.in);
        int f= RandomNumGen.randNum();
        System.out.print("Введіть імя : ");

        String name = scanner.nextLine();
        User.name(name);

        System.out.println("Let the game begin!");
//        System.out.println(f);
        DynamicArray arr = new DynamicArray(); // Створено динамічний масив

        while (true) {

            System.out.print("Введіть загадане ціле число від 0 до 100 : ");
            int userNum = scanner.nextInt();

            if (userNum < RandomNumGen.n) {

                System.out.println("Число за маленьке, введіть повторно");

                arr.add(userNum); // Додано невірне число до масиву

            } else if (userNum > RandomNumGen.n) {

                System.out.println("Число за велике, введіть повторно");

                arr.add(userNum); // Додано невірне число до масиву

            } else {

                System.out.println("Ви вгадали {" + name + "}! Купіть собі пива!");
                break;
            }
        }

        System.out.print("Невірні числа:"+"\n[");

        for (int i = 0; i < arr.size(); i++) {
            System.out.print("["+arr.get(i) + "]");
        }
        System.out.println("]");
    }


}

